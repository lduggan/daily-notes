## Videos
- [x] [Netlify child pages migration](https://www.youtube.com/watch?v=TA-VCRhCQSE)
- [x] [Slippers Video ft Tyler](https://youtu.be/HbL43BEJe7o)
- [x] [GitLab Brand video and form:](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4168)
- [x] [Slippers Versioning meeting](https://www.youtube.com/watch?v=zuvEm_CAWtg&feature=youtu.be)
- [x] [Sprint planning meeting March 1 that I left partway through](https://youtu.be/Jp95TbNBrHg)
- [x] [Git 101 video](https://youtu.be/l_P819ly2vk)
- [x] [Git 102 video](https://www.youtube.com/watch?v=A1dInTM_18Y)
- [x] [Netlify CMS blog video](https://youtu.be/kyiOq74ASUA)
- [x] [Slippers NPM package process](https://youtu.be/eV6KHQwsIOg?t=920)
- [x] [Git session with Lauren](https://www.youtube.com/watch?v=XKmJq7Fs6Lw)
- [ ] [Javi Slippers CSS session](https://youtu.be/xYU43x1n_FY)
- [ ] [Tyler and Tina discuss events template](https://youtu.be/OW1EDenckMs)
- [ ] [Tyler and Javi talk about accessibility](https://youtu.be/bE7A9IYSRJs)
- [x] [Git 101 session 4 with Laura](https://youtu.be/T_w1InssIxw)
- [x] [Tina goes through top feeders to free trial pages](https://www.youtube.com/watch?v=du-SakD8qxw)
- [ ] [Netlify CMS learning](https://youtu.be/J6A3hF3yPKg)
- [ ] [Beyond Code Session 2](https://youtu.be/RzAhBRQbyw8))

## Documentation
- [x] [ES Modules](https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/)
- [x] [Storybook Vue 3 documentation](https://storybook.js.org/blog/storybook-vue3/)
- [x] [Grid component in Storybook with React](https://dev.to/nghiemthu/complete-tutorial-grid-component-with-react-typescript-storybook-scss-2dhp) and [code](https://github.com/nghiemthu/grid-react-component)
- [x] [Grid component storybook documentation](https://people.wikimedia.org/~santhosh/storybook/?path=/docs/layout-grid--equal-width-12-columns)
- [x] [Tailwind original CSS config](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/788)
- [x] [HTML to HAML](https://html2haml.herokuapp.com/)


## Administrative

* [RRSP matching](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/110)
* [Equity grant refresh](https://gitlab.com/gitlab-org/security/www-gitlab-com/-/merge_requests/21)
* [Location factor issue](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/228)
* [Ottawa Location factor issue](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/271)
